import sys
import csv
import yaml
import datetime
import mysql.connector
from decimal import Decimal
from mysql.connector.cursor import MySQLCursorPrepared

VAT = 21


class TurnoverCalculate(object):
    def __init__(self, db):
        """
        :param db: mysql.connection as dependency
        """
        self.__db = db

    def calculate_for_day_between(self, date_from, date_to):
        """
        :param date_from: string in format %Y-%m-%d(1980-01-02)
        :param date_to: string in format %Y-%m-%d(1980-01-02)
        :return: list of calculated items
        """

        query = """SELECT 
            b.name,
            g.brand_id,
            g.date,
            sum(g.turnover) OVER(PARTITION BY g.date ORDER BY g.date) as by_date, 
            sum(g.turnover) OVER(PARTITION BY g.brand_id ORDER BY g.brand_id) as by_brand            
        FROM 
            gmv g JOIN brands b ON g.brand_id = b.id 
        WHERE 
            DAY(g.date) >= ? AND DAY(g.date) <= ? AND 
            MONTH(g.date) >= ? AND MONTH(g.date) <= ? AND
            YEAR(g.date) >= ? AND YEAR(g.date) <= ?
        """
        cursor = self.__db.cursor(cursor_class=MySQLCursorPrepared)

        cursor.execute(query, (date_from.day, date_to.day, date_from.month, date_to.month, date_from.year,
                               date_to.year))

        result = {"by_brand": {}, "by_date": {}}
        # VAT calculations
        # https://www.vero.fi/en/businesses-and-corporations/about-corporate-taxes/vat/how-to-calculate-vat/
        for row in cursor.fetchall():
            if result["by_brand"].get(row[1], None) is None:
                # need's to be decimal!!!!
                _sum = Decimal(row[4])
                taxable_amount = (VAT * _sum) / 100 + VAT

                result["by_brand"][row[1]] = (row[0], _sum, _sum - taxable_amount)

            if result["by_date"].get(row[2], None) is None:
                # need's to be decimal!!!!
                _sum = Decimal(row[3])
                taxable_amount = (VAT * _sum) / 100 + VAT
                result["by_date"][row[2]] = (row[2], _sum, _sum - taxable_amount)
        return result

    def print_as_csv(self, date_from, date_to, data):
        """
        :param date_from: string in format %Y-%m-%d(1980-01-02)
        :param date_to: string in format %Y-%m-%d(1980-01-02)
        :param data: list of selected data
        :return:
        """
        f = "{}_{}.csv".format(date_from, date_to)

        with open(f, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            writer.writerow(["Brand", "By_brand_with_VAT", "By_brand_without_VAT"])
            for row in data["by_brand"]:
                writer.writerow(data["by_brand"][row])

            writer.writerow([])

            writer.writerow(["Date", "By_date_with_VAT", "By_date_without_VAT"])
            for row in data["by_date"]:
                writer.writerow(data["by_date"][row])


def load_config(config_file):
    """

    :param config_file: full path to config file
    :return: loaded config
    """
    with open(config_file, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


def main():

    cfg = load_config("config.yaml")

    try:
        db = mysql.connector.connect(
            host=cfg['db']['host'],
            user=cfg['db']['user'],
            passwd=cfg['db']['password'],
            database=cfg['db']['database'],
            use_pure=True
        )
    except Exception as e:
        print("An error occurred: {}".format(e))
        return

    args = sys.argv
    if len(args) < 3 or len(args) > 3:
        print("Run format: app.py DATE_FROM:%Y-%m-%d DATE_TO:%Y-%m-%d")
        return

    # if date in wrong format generates an ValueError exception
    try:
        df = datetime.datetime.strptime(args[1], "%Y-%m-%d")
        dt = datetime.datetime.strptime(args[2], "%Y-%m-%d")
    except ValueError as e:
        print("An error occurred: {}".format(e))
        return

    calculator = TurnoverCalculate(db)

    calculated = calculator.calculate_for_day_between(df, dt)

    # dates are valid, so just print with user input
    calculator.print_as_csv(args[1], args[2], calculated)


if __name__ == "__main__":
    main()
