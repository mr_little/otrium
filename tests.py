import datetime
import unittest
from unittest import mock
from decimal import Decimal
from app import TurnoverCalculate


class Test(unittest.TestCase):

    def test_calculation(self):

        mock_connection = mock.MagicMock()
        mock_cursor = mock.MagicMock()
        mock_connection.cursor.return_value = mock_cursor
        mock_cursor.fetchall.return_value = [
            ('O-Brand', 1, datetime.date(2018, 5, 1), '100730.63', '21666.70'),
            ('O-Brand', 1, datetime.date(2018, 5, 2), '94545.22', '21666.70'),
            ('T-Brand', 2, datetime.date(2018, 5, 1), '100730.63', '63064.16'),
            ('T-Brand', 2, datetime.date(2018, 5, 2), '94545.22', '63064.16'),
            ('R-Brand', 3, datetime.date(2018, 5, 1), '100730.63', '43797.97'),
            ('R-Brand', 3, datetime.date(2018, 5, 2), '94545.22', '43797.97'),
            ('I-Brand', 4, datetime.date(2018, 5, 1), '100730.63', '13798.64'),
            ('I-Brand', 4, datetime.date(2018, 5, 2), '94545.22', '13798.64'),
            ('U-Brand', 5, datetime.date(2018, 5, 1), '100730.63', '52948.38'),
            ('U-Brand', 5, datetime.date(2018, 5, 2), '94545.22', '52948.38')
        ]

        df = datetime.datetime.strptime("2018-05-01", "%Y-%m-%d")
        dt = datetime.datetime.strptime("2018-05-02", "%Y-%m-%d")

        excpect = {
            'by_brand': {
                1: ('O-Brand', Decimal('21666.70'), Decimal('17095.693')),
                2: ('T-Brand', Decimal('63064.16'), Decimal('49799.6864')),
                3: ('R-Brand', Decimal('43797.97'), Decimal('34579.3963')),
                4: ('I-Brand', Decimal('13798.64'), Decimal('10879.9256')),
                5: ('U-Brand', Decimal('52948.38'), Decimal('41808.2202'))},
            'by_date': {
                datetime.date(2018, 5, 1): (datetime.date(2018, 5, 1), Decimal('100730.63'), Decimal('79556.1977')),
                datetime.date(2018, 5, 2): (datetime.date(2018, 5, 2), Decimal('94545.22'), Decimal('74669.7238'))
            }
        }

        item = TurnoverCalculate(mock_connection)
        result = item.calculate_for_day_between(df, dt)
        self.assertEqual(result, excpect)


if __name__ == "__main__":
    unittest.main()





